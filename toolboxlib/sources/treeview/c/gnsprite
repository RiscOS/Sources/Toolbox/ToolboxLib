/************************************************************************
 * � Acorn Computers Ltd, 1994.                                         *
 *                                                                      *
 * It may be freely used to create executable images for saleable       *
 * products but cannot be sold in source form or as an object library   *
 * without the prior written consent of Acorn Computers Ltd.            *
 *                                                                      *
 * If this file is re-distributed (even if modified) it should retain   *
 * this copyright notice.                                               *
 *                                                                      *
 ************************************************************************/


/*
 * Name        : gnsprite.c
 * Purpose     : Veneer for method TreeView_GetNodeSprite
 * Description : Reads either of the sprite names associated with the current node
 */


#include "kernel.h"
#include "toolbox.h"
#include "gadgets.h"
#include "treeview.h"




/*
 * Name        : treeview_get_node_sprite
 * Description : Reads either of the sprite names associated with the current node
 * In          : unsigned int flags
 *               ObjectId window
 *               ComponentId gadget
 *               char *buffer
 *               int buff_size
 * Out         : int *nbytes
 * Returns     : pointer to error block
 */

extern _kernel_oserror *treeview_get_node_sprite ( unsigned int flags,
                                                   ObjectId window,
                                                   ComponentId gadget,
                                                   char *buffer,
                                                   int buff_size,
                                                   int *nbytes
                                                 )
{
_kernel_swi_regs r;
_kernel_oserror *e;

  r.r[0] = flags;
  r.r[1] = (int) window;
  r.r[2] = TreeView_GetNodeSprite;
  r.r[3] = (int) gadget;
  r.r[4] = (int) buffer;
  r.r[5] = (int) buff_size;
  if((e = _kernel_swi(Toolbox_ObjectMiscOp,&r,&r)) == NULL)
  {
    if(nbytes != NULL) *nbytes = (int) r.r[0];
  }

  return(e);
}

