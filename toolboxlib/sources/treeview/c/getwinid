/************************************************************************
 * � Acorn Computers Ltd, 1994.                                         *
 *                                                                      *
 * It may be freely used to create executable images for saleable       *
 * products but cannot be sold in source form or as an object library   *
 * without the prior written consent of Acorn Computers Ltd.            *
 *                                                                      *
 * If this file is re-distributed (even if modified) it should retain   *
 * this copyright notice.                                               *
 *                                                                      *
 ************************************************************************/


/*
 * Name        : getwinid.c
 * Purpose     : Veneer for method TreeView_GetWindowId
 * Description : Returns the Window object for the window in which the tree resides
 */


#include "kernel.h"
#include "toolbox.h"
#include "gadgets.h"
#include "treeview.h"




/*
 * Name        : treeview_get_window_id
 * Description : Returns the Window object for the window in which the tree resides
 * In          : unsigned int flags
 *               ObjectId window
 *               ComponentId gadget
 * Out         : ObjectId *id
 * Returns     : pointer to error block
 */

extern _kernel_oserror *treeview_get_window_id ( unsigned int flags,
                                                 ObjectId window,
                                                 ComponentId gadget,
                                                 ObjectId *id
                                               )
{
_kernel_swi_regs r;
_kernel_oserror *e;

  r.r[0] = flags;
  r.r[1] = (int) window;
  r.r[2] = TreeView_GetWindowId;
  r.r[3] = (int) gadget;
  if((e = _kernel_swi(Toolbox_ObjectMiscOp,&r,&r)) == NULL)
  {
    if(id != NULL) *id = (ObjectId) r.r[0];
  }

  return(e);
}

